# Aufgabe 2

## Nur Klausurschreiber:

Klonen Sie das Projekt: https://stbaeumer@bitbucket.org/stbaeumer/n27gw12-klausur1-formular.git

Bearbeiten Sie das vorliegende Projekt so, dass bei Klick auf den Button der Name des Schülers aus dem Input initialisiert und im Terminal ausgegeben wird. Im Terminal soll dann stehen: ```Schüler angelegt: Pit```. Weiterhin soll eine zweite Eigenschaft initialisiert werden. Da diese Eigenschaft für alle Schüler festgelegt ist, ist kein weiteres Input für die Initialisierung notwendig. Die Eigenschaft heißt ```Schule``` und der Eigenschaftswert heißt ```BKB```. Die Eigenschaft ist eine Konstante.

Sie müssen in der ```server.js``` eine entsprechende Klasse anlegen, deklarieren, instanziiern usw.

#### Schreiben Sie Ihre Lösung handschriftlich hier auf diesen Zettel. Nur der Zettel wird eingesammelt und bewertet. Geben Sie unbedingt die genaue Zeile an, in der Sie ihren Quelltext einfügen!

## Nur Testschreiber:
Testschreiber müssen lediglich eine Klassendefinition für eine Klasse namens ```Lehrer``` hier aufschreiben. Definieren Sie 4 relevante Eigenschaften.

#### Schreiben Sie Ihre Lösung handschriftlich auf diesen Zettel. Nur dieser Zettel wird eingesammelt und bewertet!







